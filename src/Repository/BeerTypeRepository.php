<?php

namespace App\Repository;

use App\Entity\BeerType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BeerType|null find($id, $lockMode = null, $lockVersion = null)
 * @method BeerType|null findOneBy(array $criteria, array $orderBy = null)
 * @method BeerType[]    findAll()
 * @method BeerType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BeerTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BeerType::class);
    }

    // /**
    //  * @return BeerType[] Returns an array of BeerType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BeerType
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
