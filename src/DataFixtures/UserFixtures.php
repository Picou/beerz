<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('Kronenbourg');
        $user->setPrenom('John');
        $user->setNom('Doe');
        $user->setEmail('john.doe@kronenbourg.org');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_kronenbourg', $user);

        $user = new User();
        $user->setUsername('BrewAge');
        $user->setPrenom('John');
        $user->setNom('Doe');
        $user->setEmail('john.doe@brewage.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_brewage', $user);

        $user = new User();
        $user->setUsername('Brouwerij Bosteels');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@brouwerij.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_brouwerijbosteels', $user);

        $user = new User();
        $user->setUsername('Brouwerij De Molen');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@brouwerij.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_brouwerijdemolen', $user);

        $user = new User();
        $user->setUsername('Brouwerij Breugem');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@brouwerij.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_brouwerijbreugem', $user);

        $user = new User();
        $user->setUsername('BRLO');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@brlo.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_brlo', $user);

        $user = new User();
        $user->setUsername('MontBlanc');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@montblancbeer.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_montblanc', $user);

        $user = new User();
        $user->setUsername('Baladin');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@baladin.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_baladin', $user);

        $user = new User();
        $user->setUsername('Goudale');
        $user->setPrenom('Brew');
        $user->setNom('Beer');
        $user->setEmail('brew@goudale.com');
        $user->setRoles(['ROLE_PRODUCER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_goudale', $user);

        $user = new User();
        $user->setUsername('Admin');
        $user->setPrenom('Admin');
        $user->setNom('Admin');
        $user->setEmail('admin@beerz.com');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_admin', $user);

        $user = new User();
        $user->setUsername('Consumer');
        $user->setPrenom('Consumer');
        $user->setNom('Consumer');
        $user->setEmail('consumer@beerz.com');
        $user->setRoles(['ROLE_USER']);
        $user->setAddress('Boulevard de l\'Europe 67212 OBERNAI');
        $user->setTel(0601020304);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                'password'
            )
        );
        $manager->persist($user);
        $this->addReference('user_consumer', $user);

        $manager->flush();
    }
}
