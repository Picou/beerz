<?php

namespace App\DataFixtures;

use App\Entity\Beer;
use App\Entity\User;
use App\Repository\BeerTypeRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BeerFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var BeerTypeRepository
     */
    private $beerTypeRepository;

    public function __construct(UserRepository $userRepository, BeerTypeRepository $beerTypeRepository)
    {
        $this->userRepository = $userRepository;
        $this->beerTypeRepository = $beerTypeRepository;
    }

    public function load(ObjectManager $manager)
    {
        $beer = new Beer();
        $beer->setName('Grimbergen Blond');
        $beer->setImg('grimbergen.jpg');
        $beer->setDegre(6.7);
        $beer->setPrice(1.89);
        $beer->setDescription('Une bière blonde légère aux notes de fruits mûrs, parfaitement limpide, à la mousse dense couleur crème.');
        $beer->setProducer($this->getReference('user_kronenbourg'));
        $beer->setBeerType($this->getReference('beer_blonde'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Grimbergen Ambrée');
        $beer->setImg('grimbergenambree.jpg');
        $beer->setDegre(6.5);
        $beer->setPrice(1.86);
        $beer->setDescription('Une bière brune haute fermentation aux notes douces-amères, rehaussées d’un soupçon de caramel et de pruneaux, avec des sortes de houblon et de malt fermentées deux fois.');
        $beer->setProducer($this->getReference('user_kronenbourg'));
        $beer->setBeerType($this->getReference('beer_ambree'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Affenkönig');
        $beer->setImg('affenkonig.jpg');
        $beer->setDegre(8.2);
        $beer->setPrice(3.49);
        $beer->setDescription('Au nez, on sent directement les arômes très présents apportés par les houblons. Une explosion de fruits : pamplemousse et fruits exotiques.');
        $beer->setProducer($this->getReference('user_brewage'));
        $beer->setBeerType($this->getReference('beer_ipa'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Tripel Karmeliet');
        $beer->setImg('tripelkarmeliet.jpg');
        $beer->setDegre(8.4);
        $beer->setPrice(2.00);
        $beer->setDescription('Brassée avec trois céréales, l’orge, l’avoine et le froment, cette bière dévoile une robe dorée lumineuse et présente une généreuse mousse.');
        $beer->setProducer($this->getReference('user_brouwerijbosteels'));
        $beer->setBeerType($this->getReference('beer_tripel'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('De Molen Hemel & Aarde');
        $beer->setImg('demolen.jpg');
        $beer->setDegre(10.0);
        $beer->setPrice(4.30);
        $beer->setDescription('Brune avec une légère mousse brune. Cette bière présente des arômes puissants de goudron, de gazon et de fumée avec une longue tenue en bouche.');
        $beer->setProducer($this->getReference('user_brouwerijdemolen'));
        $beer->setBeerType($this->getReference('beer_stout'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Breugem Saens Zoentje');
        $beer->setImg('saenszoentje.jpg');
        $beer->setDegre(6.2);
        $beer->setPrice(2.00);
        $beer->setDescription('Le produit phare de la brasserie, l\'une des favorites des buveurs de bières Breugem, est de couleur dorée ambrée. Plus fraîche qu\'une bock de Printemps et moins amère qu\'une IPA.');
        $beer->setProducer($this->getReference('user_brouwerijbreugem'));
        $beer->setBeerType($this->getReference('beer_blonde'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('BRLO German IPA');
        $beer->setImg('brlo.jpg');
        $beer->setDegre(7.0);
        $beer->setPrice(2.50);
        $beer->setDescription('Une India Pale Ale avec beaucoup de caractère, brassée avec uniquement des houblons allemands. Beaucoup d’arômes de pamplemousse et une finale en bouche amère.');
        $beer->setProducer($this->getReference('user_brlo'));
        $beer->setBeerType($this->getReference('beer_ipa'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Rousse du Mont Blanc');
        $beer->setImg('roussemontblanc.jpg');
        $beer->setDegre(6.5);
        $beer->setPrice(2.70);
        $beer->setDescription('Cette bière à la robe ambrée recèle des arômes maltés. Cette bière est douce et légèrement sucrée, avec des arômes de houblon.');
        $beer->setProducer($this->getReference('user_montblanc'));
        $beer->setBeerType($this->getReference('beer_ambree'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Baladin Nora');
        $beer->setImg('baladinnora.jpg');
        $beer->setDegre(6.8);
        $beer->setPrice(3.50);
        $beer->setDescription('Une bière originale à la couleur orangée avec du Kamut, une ancienne graine d\'Egypte,du gingembre et de la myrrhe. Les épices ont le rôle principal.');
        $beer->setProducer($this->getReference('user_baladin'));
        $beer->setBeerType($this->getReference('beer_ambree'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Goudale Ambrée');
        $beer->setImg('goudaleambree.jpg');
        $beer->setDegre(7.2);
        $beer->setPrice(1.65);
        $beer->setDescription('Une couleur de bois, avec un col de mousse blanc medium. Un caractère malté avec de légères notes fruitées et de caramel, et une amertume modérée.');
        $beer->setProducer($this->getReference('user_goudale'));
        $beer->setBeerType($this->getReference('beer_ambree'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Divine');
        $beer->setImg('ladivine.jpg');
        $beer->setDegre(8.5);
        $beer->setPrice(1.90);
        $beer->setDescription('Une robe or foncé, un col de mousse blanc. En bouche, cette bière est douce, légèrement alcoolisée avec des arômes de fruits et une amertume modérée.');
        $beer->setProducer($this->getReference('user_goudale'));
        $beer->setBeerType($this->getReference('beer_blonde'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Goudale IPA');
        $beer->setImg('goudaleipa.jpg');
        $beer->setDegre(7.2);
        $beer->setPrice(2.20);
        $beer->setDescription('Une IPA de style belge aux arômes de fruits tropicaux. Cette bière est douce, avec une base maltée et une amertume modérée.');
        $beer->setProducer($this->getReference('user_goudale'));
        $beer->setBeerType($this->getReference('beer_ipa'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('Triple Secret Des Moines Blonde');
        $beer->setImg('triplesecret.jpg');
        $beer->setDegre(8.0);
        $beer->setPrice(2.05);
        $beer->setDescription('Une bière blonde puissante avec de délicats arômes de fruits. En bouche, on y retrouve des épices et une légère amertume.');
        $beer->setProducer($this->getReference('user_goudale'));
        $beer->setBeerType($this->getReference('beer_tripel'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Verte du Mont Blanc');
        $beer->setImg('vertemontblanc.jpg');
        $beer->setDegre(5.9);
        $beer->setPrice(2.70);
        $beer->setDescription('Bière verte aux saveurs herbales et légèrement amères. Au nez, elle est mentholée. La finale en bouche est courte et assez herbale.');
        $beer->setProducer($this->getReference('user_montblanc'));
        $beer->setBeerType($this->getReference('beer_blonde'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Cristal IPA du Mont Blanc');
        $beer->setImg('cristalipamontblanc.jpg');
        $beer->setDegre(4.7);
        $beer->setPrice(2.80);
        $beer->setDescription('Une IPA claire comme du cristal, brassée avec l\'eau du Mont Blanc. Légèrement épicée, florale, houblonnée et très rafraîchissante!');
        $beer->setProducer($this->getReference('user_montblanc'));
        $beer->setBeerType($this->getReference('beer_ipa'));
        $manager->persist($beer);

        $beer = new Beer();
        $beer->setName('La Blonde du Mont Blanc');
        $beer->setImg('blondemontblanc.jpg');
        $beer->setDegre(5.8);
        $beer->setPrice(2.60);
        $beer->setDescription('Cette bière blonde, brassée avec une eau de source puisée à même le flanc du Mont-Blanc, est douce et pleine de saveurs.');
        $beer->setProducer($this->getReference('user_montblanc'));
        $beer->setBeerType($this->getReference('beer_blonde'));
        $manager->persist($beer);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            BeerTypeFixtures::class,
        );
    }
}
