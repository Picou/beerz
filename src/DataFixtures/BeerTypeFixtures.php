<?php

namespace App\DataFixtures;

use App\Entity\BeerType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BeerTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $beerType = new BeerType();
        $beerType->setName('Ambrée');
        $beerType->setImg('ambree.jpg');
        $manager->persist($beerType);
        $this->addReference('beer_ambree', $beerType);

        $beerType = new BeerType();
        $beerType->setName('Blonde');
        $beerType->setImg('blonde.jpg');
        $manager->persist($beerType);
        $this->addReference('beer_blonde', $beerType);

        $beerType = new BeerType();
        $beerType->setName('Stout');
        $beerType->setImg('stout.jpg');
        $manager->persist($beerType);
        $this->addReference('beer_stout', $beerType);

        $beerType = new BeerType();
        $beerType->setName('IPA');
        $beerType->setImg('ipa.jpg');
        $manager->persist($beerType);
        $this->addReference('beer_ipa', $beerType);

        $beerType = new BeerType();
        $beerType->setName('Tripel');
        $beerType->setImg('tripel.jpg');
        $manager->persist($beerType);
        $this->addReference('beer_tripel', $beerType);

        $manager->flush();
    }
}
