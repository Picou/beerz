<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190709200805 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer ADD beer_type_id INT NOT NULL, ADD producer_id INT NOT NULL, DROP id_producer, DROP id_category');
        $this->addSql('ALTER TABLE beer ADD CONSTRAINT FK_58F666ADA3829862 FOREIGN KEY (beer_type_id) REFERENCES beer_type (id)');
        $this->addSql('ALTER TABLE beer ADD CONSTRAINT FK_58F666AD89B658FE FOREIGN KEY (producer_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_58F666ADA3829862 ON beer (beer_type_id)');
        $this->addSql('CREATE INDEX IDX_58F666AD89B658FE ON beer (producer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer DROP FOREIGN KEY FK_58F666ADA3829862');
        $this->addSql('ALTER TABLE beer DROP FOREIGN KEY FK_58F666AD89B658FE');
        $this->addSql('DROP INDEX IDX_58F666ADA3829862 ON beer');
        $this->addSql('DROP INDEX IDX_58F666AD89B658FE ON beer');
        $this->addSql('ALTER TABLE beer ADD id_producer INT NOT NULL, ADD id_category INT NOT NULL, DROP beer_type_id, DROP producer_id');
    }
}
