<?php

namespace App\Form;

use App\Entity\BeerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewBeerTypeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un nom'
                    ]),
                ],
                'required' => true
            ])
            ->add('img', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize' => '8192k',
                    ]),
                    new NotBlank([
                        'message' => 'Entrez une image'
                    ])
                ],
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BeerType::class,
        ]);
    }
}
