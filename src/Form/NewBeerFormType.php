<?php

namespace App\Form;

use App\Entity\Beer;
use App\Entity\BeerType;
use App\Repository\BeerTypeRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewBeerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un nom'
                    ]),
                ],
                'required' => true
            ])
            ->add('price', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un prix'
                    ]),
                ],
                'required' => true
            ])
            ->add('img', FileType::class, [
                'constraints' => [
                    new File([
                        'maxSize' => '8192k',
                    ]),
                    new NotBlank([
                        'message' => 'Entrez une image'
                    ])
                ],
                'required' => true
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez une description'
                    ]),
                ],
                'required' => true
            ])
            ->add('beerType', EntityType::class, [
                'class' => BeerType::class,
                'choice_label' => 'name',
                'required' => true
            ])
            ->add('degre')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Beer::class,
        ]);
    }
}
