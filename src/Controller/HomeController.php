<?php

namespace App\Controller;

use App\Repository\BeerTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(BeerTypeRepository $repository)
    {
        $beerTypes = $repository->findAll();

        return $this->render('home/index.html.twig', [
            'beerTypes' => $beerTypes,
        ]);
    }
}
