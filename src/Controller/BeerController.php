<?php

namespace App\Controller;

use App\Entity\Beer;
use App\Entity\Cart;
use App\Entity\Comment;
use App\Form\CartFormType;
use App\Form\NewBeerFormType;
use App\Form\NewCommentFormType;
use App\Repository\BeerRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class BeerController extends AbstractController
{
    /**
     * @Route("/beer", name="beer")
     */
    public function index()
    {
        return $this->render('beer/index.html.twig', [
            'controller_name' => 'BeerController',
        ]);
    }

    /**
     * @Route("/beer/{id}", name="beer_show")
     * @param $id
     * @param BeerRepository $beerRepository
     * @param CommentRepository $commentRepository
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function showBeer($id, BeerRepository $beerRepository, CommentRepository $commentRepository, Request $request, EntityManagerInterface $entityManager)
    {
        $beer = $beerRepository->find($id);

        // Create the "Add to cart" form
        $cart = new Cart();
        $addToCartForm = $this->createForm(CartFormType::class, $cart);
        $addToCartForm->handleRequest($request);

        // Create the comments form
        $newComment = new Comment();
        $user = $this->getUser();
        $form = $this->createForm(NewCommentFormType::class, $newComment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newComment->setCreatedAt(new \DateTime());
            $newComment->setBeer($beer);
            $newComment->setUser($user);

            $entityManager->persist($newComment);
            $entityManager->flush();


            return $this->redirectToRoute("beer_show", ['id' => $id]);
        }
        else if($addToCartForm->isSubmitted() && $addToCartForm->isValid()) {
            $cart->setBeer($beer);
            $cart->setUser($user);

            $entityManager->persist($cart);
            $entityManager->flush();
            return $this->redirectToRoute('cart');
        }

        $comments = $commentRepository->findBy(array('beer' => $id), array('createdAt' => 'DESC'));

        return $this->render('beer/beer.html.twig', [
            'beer' => $beer,
            'comments' => $comments,
            'NewCommentForm' => $form->createView(),
            'addToCartForm' => $addToCartForm->createView()
        ]);
    }

    /**
     * @Route("/beers", name="beers")
     */
    public function beersPage(Request $request, BeerRepository $beerRepository)
    {
        $form = $this->createFormBuilder(null)
            ->add('search', SearchType::class, ['required' => false])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$name = $request->query->get('search');
            $name = $form["search"]->getData();
            $beers = $beerRepository->findLike($name);
            //$beers = $beerRepository->findOneBySomeField($name);
        }
        else {
            $beers = $beerRepository->findAll();
        }

        return $this->render('beer/beers.html.twig', [
            'searchForm' => $form->createView(), 'beers' => $beers
        ]);
    }

}
