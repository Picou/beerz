<?php

namespace App\Controller;

use App\Entity\Beer;
use App\Form\NewBeerFormType;
use App\Repository\BeerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProducerController extends AbstractController
{

    /**
     * @Route("/producer/beers", name="producer_beers")
     */
    public function beerList(BeerRepository $beerRepository)
    {
        $producer = $this->getUser();
        $beersProducer = $beerRepository->findBy(['producer' => $producer->getId()]);

        return $this->render('producer/beers.html.twig', [
            'beers' => $beersProducer,
        ]);
    }

    /**
     * @Route("/producer/new-beer", name="create_beer")
     */
    public function newBeer(Request $request)
    {
        $beer = new Beer();
        $form = $this->createForm(NewBeerFormType::class, $beer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $beer->setProducer($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($beer);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('producer_beers');
        }

        return $this->render('producer/new-beer.html.twig', [
            'newBeerForm' => $form->createView(),
        ]);
    }
}
