<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Form\CartFormType;
use App\Repository\CartRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     * @param CartRepository $cartRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cart(CartRepository $cartRepository)
    {
        $cart = $cartRepository->findBy(['user' => $this->getUser()]);

        $total = 0;
        foreach ($cart as $item) {
            $total += $item->getQuantity() * $item->getBeer()->getPrice();
        }

        return $this->render('user/cart.html.twig', [
            'cart' => $cart,
            'total' => $total
        ]);
    }

    /**
     * @Route("/cart/item/delete", name="delete_item_cart", methods="POST")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param CartRepository $cartRepository
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteItemFromCart(Request $request, EntityManagerInterface $entityManager, CartRepository $cartRepository) {

        $cartId = $request->request->get('cart');
        $cart = $cartRepository->find($cartId);
        if($cart->getUser() === $this->getUser()) {
            $entityManager->remove($cart);
            $entityManager->flush();
        }

        if($request->isXmlHttpRequest()) {
            return new JsonResponse('ok');
        } else {
            return $this->redirectToRoute('cart');
        }
    }

    /**
     * @Route("/cart/order", name="cart_order", methods="POST")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param CartRepository $cartRepository
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function orderCart(Request $request, EntityManagerInterface $entityManager, CartRepository $cartRepository) {

        $items = $cartRepository->findBy(['user' => $this->getUser()]);
        foreach ($items as $item) {
            $entityManager->remove($item);
        }
        $entityManager->flush();

        if($request->isXmlHttpRequest()) {
            return new JsonResponse('ok');
        } else {
            return $this->redirectToRoute('cart');
        }
    }
}