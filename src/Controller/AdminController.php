<?php

namespace App\Controller;

use App\Repository\BeerTypeRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    /**
     * @Route("/admin/beer-types", name="beer_types")
     */
    public function beerTypesList(BeerTypeRepository $beerTypeRepository)
    {
        $beerTypes = $beerTypeRepository->findAll();

        return $this->render('admin/beer-types.html.twig', [
            'beerTypes' => $beerTypes,
        ]);
    }

    /**
     * @Route("/admin/user-mngt", name="user_mngt")
     */
    public function userManagement(UserRepository $userRepository)
    {
        $users = $userRepository->findAll();
        foreach ($users as $user) {
            if(in_array("ROLE_ADMIN", $user->getRoles())) {
                $key = array_search($user, $users);
                unset($users[$key]);
            }
        }

        return $this->render('admin/user-mngt.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/delete-user/{id}", name="delete_user")
     */
    public function deleteUser($id, UserRepository $userRepository)
    {
        $user = $userRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute("user_mngt");
    }
}
