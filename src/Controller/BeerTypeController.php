<?php

namespace App\Controller;

use App\Entity\BeerType;
use App\Entity\User;
use App\Form\NewBeerTypeFormType;
use App\Repository\BeerRepository;
use App\Repository\BeerTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BeerTypeController extends AbstractController
{
    /**
    * @Route("/admin/delete-beer-type/{id}", name="delete_beer_type")
    */
    public function deleteBeerType($id, BeerTypeRepository $beerTypeRepository)
    {
        $beerType = $beerTypeRepository->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($beerType);
        $em->flush();

        return $this->redirectToRoute("beer_types");
    }

    /**
     * @Route("/admin/new-beer-type", name="create_beer_type")
     */
    public function newBeerType(Request $request)
    {
        $beerType = new BeerType();
        $form = $this->createForm(NewBeerTypeFormType::class, $beerType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $typeImg = $form['img']->getData();
            if($typeImg) {
                $originalNameImg = pathinfo($typeImg->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalNameImg);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$typeImg->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $typeImg->move(
                        $this->getParameter('beertype_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $beerType->setImg($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($beerType);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('beer_types');
        }

        return $this->render('admin/new-beer-type.html.twig', [
            'newBeerForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/beer-type/{id}", name="show_type")
     */
    public function showBeersFromType($id, BeerRepository $beerRepository, BeerTypeRepository $beerTypeRepository)
    {
        $beerType = $beerTypeRepository->find($id);
        $beers = $beerRepository->findAll();
        foreach ($beers as $beer) {
            if($beer->getBeerType()->getId() != $id) {
                $key = array_search($beer, $beers);
                unset($beers[$key]);
            }
        }


        return $this->render('beer/categories.html.twig', [
            'beers' => $beers, 'name_type' => $beerType->getName()
        ]);
    }
}
